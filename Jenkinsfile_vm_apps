#!groovy
pipeline {
    agent any

    parameters {
        string(name: 'VM_NAME', defaultValue: 'skip', description: 'nom de la VM')
        string(name: 'DOMAIN_NAME', defaultValue: 'aeioui.org', description: 'only for haproxy_domain')
        string(name: 'verbose', defaultValue: '-v', description: 'niveau de verbose')


        booleanParam(defaultValue: false, description: '', name: 'consul')
        booleanParam(defaultValue: false, description: '', name: 'vault')
        booleanParam(defaultValue: false, description: '', name: 'dns')
        booleanParam(defaultValue: false, description: '', name: 'haproxy')
        booleanParam(defaultValue: false, description: '', name: 'postgres')
        booleanParam(defaultValue: false, description: '', name: 'pgpool')
        booleanParam(defaultValue: false, description: '', name: 'mongodb')
        booleanParam(defaultValue: false, description: '', name: 'fichier_hosts')
        booleanParam(defaultValue: false, description: '', name: 'agent_terraform')        
        booleanParam(defaultValue: false, description: '', name: 'agent_kubectl')
        booleanParam(defaultValue: false, description: '', name: 'agent_filebeat')
        booleanParam(defaultValue: false, description: '', name: 'haproxy_domain')
        booleanParam(defaultValue: false, description: '', name: 'rc_local_service')
        booleanParam(defaultValue: false, description: '', name: 'vpn_pfsense')
        booleanParam(defaultValue: false, description: '', name: 'add_user')
        booleanParam(defaultValue: false, description: '', name: 'nodejs')
    }

    options {
        ansiColor('xterm')
    }

    stages {
        stage('Checkout xenserver.git') {
            steps {
                git branch: 'master',
                        url: 'https://heliocoeur:pBryYHDaXkAKLKfb8kuz@gitlab.com/heliocoeur/xenserver.git'
            }
        }

        stage('installation de consul') {
            when {
                expression { params.consul }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_consul.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('installation de vault') {
            when {
                expression { params.vault }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_vault.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('Installation du dns') {
            when {
                expression { params.dns }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_dns.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('Installation de haproxy') {
            when {
                expression { params.haproxy }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_haproxy.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('Installation de postgres') {
            when {
                expression { params.postgres }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_postgres.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('Installation de mongodb') {
            when {
                expression { params.mongodb }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_mongodb.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Installation de l agent filebeat') {
            when {
                expression { params.agent_filebeat }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_agent_filebeat.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Build haproxy_domain') {
            when {
                expression { params.haproxy_domain }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_haproxy_domain.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP} DOMAIN_NAME=${params.DOMAIN_NAME} \"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Build rc_local_service') {
            when {
                expression { params.rc_local_service }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_rc_local_service.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP} DOMAIN_NAME=${params.DOMAIN_NAME} \"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Build vpn_pfsense') {
            when {
                expression { params.vpn_pfsense }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_vpn_pfsense.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP} DOMAIN_NAME=${params.DOMAIN_NAME} \"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Build terraform') {
            when {
                expression { params.agent_terraform }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_agent_terraform.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP} DOMAIN_NAME=${params.DOMAIN_NAME} \"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('Build user') {
            when {
                expression { params.add_user }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: '${WORKSPACE}/playbooks/xenserver_add_user.yml',
                        extras: "-e \"VM_NAME=${params.VM_NAME}\"  ${params.verbose}",
                        colorized: true)
            }
        }

        stage('installation de nodejs') {
            when {
                expression { params.nodejs }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_nodejs.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP}\"  ${params.verbose}",
                        colorized: true)
            }
        }
        stage('installation de pgpool') {
            when {
                expression { params.pgpool }
            }
            steps {
                ansiblePlaybook(
                        credentialsId: 'root_center',
                        inventory: "${WORKSPACE}/hosts",
                        playbook: "${WORKSPACE}/playbooks/xenserver_pgpool.yml",
                        extras: "-e \"VM_NAME=${params.VM_NAME} VM_IP=${params.VM_IP}\"  ${params.verbose}",
                        colorized: true)
            }
        }

    } //stages
} //pipeline





