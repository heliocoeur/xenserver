const { createServer } = require('http')
const { readFileSync } = require('fs')

const server = createServer(({ url }, response) => {
const image = readFileSync("satyre_paint.jpg")
response.writeHead(200, { 'Content-Type': 'image/jpg' })
response.end(image, 'binary')
})


server.listen(443, () => console.log('Serveur démarré : http://localhost:443/'))




//// app.js
//const http = require("http")
//
//const port = 3000
//
//const requestHandler = (request, response) => {
//  console.log(request.url)
//  response.end('Hello Node.js Server!')
//}
//
//const server = http.createServer(requestHandler)
//
//server.listen(port, (err) => {
//  if (err) {
//    return console.log('something bad happened', err)
//  }
//
//  console.log(`server is listening on ${port}`)
//})