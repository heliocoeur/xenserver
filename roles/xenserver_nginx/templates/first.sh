# Use this command if a webserver is already running with the webroot
# at /var/www/html.
certbot-auto certonly \
  --agree-tos \
  --non-interactive \
  --text \
  --rsa-key-size 4096 \
  --email frederick.loubli@outlook.com \
  --webroot-path /var/www/html \
  --domains "aeioui.org, www.aeioui.org, aeioui.ovh, www.aeioui.ovh, jou0eur.ovh, www.jou0eur.ovh"