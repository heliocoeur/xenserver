create database frederick;
create user frederick with encrypted password 'Ragnarok/09';
grant all privileges on DATABASE * to frederick;

create database lamaprod;
create user lamaprod with encrypted password 'Ragnarok/09';
grant all privileges on DATABASE * to lamaprod;