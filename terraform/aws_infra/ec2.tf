resource "aws_key_pair" "admin" {
   key_name   = "admin"
   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCf8AuiU2D9HLOFCpZEZr2h6ltTBxdQRz/Hwvi9BxgOOGkX3mTFL4Uf+WmilF4/w1hqy5znSTxdtfjrmZqw0I39Z8S8tZ7oODTGOan7RSX9L4ERd1S72yukLRjVUIIm3L7fG/toPDIrcwZOURaqJo+qj4iMvAeTr1uDQ+4CKz/Wk3wsicC01kEhYf07N3/Q0P+EJSe9aA1BejhwYYYBnzm3x56DF9MeD8EleWZja0BDmwr+KxE49+U+UNHcOUYBzR1Zli4kizF2OvWRdHnNJvBpuZ32gIC2yFbEhTdDM+KAj2Npn/n1WGeGiC80A9gtbDfCYNNgscXzd/ObP+PjMXxd root@frederick"
 }
 resource "aws_instance" "server1" {
   ami           = "ami-0bca4c17a36b9c14e"
   instance_type = "t2.micro"
   key_name      = "admin"
 }

  resource "aws_instance" "server2" {
   ami           = "ami-0bca4c17a36b9c14e"
   instance_type = "t2.micro"
   key_name      = "admin"
 }

