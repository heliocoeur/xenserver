resource "aws_alb" "myfirstlb" {
	name		=	"myfirstlb"
	internal	=	false
    load_balancer_type = "application"
	subnets		=	["subnet-06d8ff6f", "subnet-4475f609", "subnet-b7e698cc"]
	enable_deletion_protection	=	false
}

resource "aws_alb_target_group" "myfirstlbgroup" {
	name	= "myfirstlbgroup"
	vpc_id	= "vpc-0cf5cd65"
	port	= "443"
	protocol	= "HTTPS"
	health_check {
                path = "/"
                port = "80"
                protocol = "HTTP"
                healthy_threshold = 2
                unhealthy_threshold = 2
                interval = 5
                timeout = 4
                matcher = "200-308"
        }
}

resource "aws_alb_target_group_attachment" "alb_backend-01_http" {
  target_group_arn = "${aws_alb_target_group.myfirstlbgroup.arn}"
  target_id        = "${aws_instance.server1.id}"
  port             = 443
}

resource "aws_alb_target_group_attachment" "alb_backend-02_http" {
  target_group_arn = "${aws_alb_target_group.myfirstlbgroup.arn}"
  target_id        = "${aws_instance.server2.id}"
  port             = 443
}

resource "aws_alb_listener" "alb_front_http" {
	load_balancer_arn	=	"${aws_alb.myfirstlb.arn}"
	port			=	"80"
	protocol		=	"HTTP"
	# ssl_policy		=	"ELBSecurityPolicy-2016-08"
	# certificate_arn		=	"arn:aws:acm:eu-west-3:926041386529:certificate/c7fca8f1-e56d-4cde-9e01-f7c6aa9883fc"

	default_action {
		target_group_arn	=	"${aws_alb_target_group.myfirstlbgroup.arn}"
		type			=	"forward"
	}
}

resource "aws_alb_listener" "alb_front_https" {
	load_balancer_arn	=	"${aws_alb.myfirstlb.arn}"
	port			=	"443"
	protocol		=	"HTTPS"
	ssl_policy		=	"ELBSecurityPolicy-2016-08"
	certificate_arn		=	"arn:aws:acm:eu-west-3:926041386529:certificate/c7fca8f1-e56d-4cde-9e01-f7c6aa9883fc"

	default_action {
		target_group_arn	=	"${aws_alb_target_group.myfirstlbgroup.arn}"
		type			=	"forward"
	}
}
