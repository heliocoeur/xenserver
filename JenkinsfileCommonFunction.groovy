#!/usr/bin/env groovy

class Info {
    String vmip
}

INFO_BY_APP =   ['webapp': new Info(vmip: '192.168.0.99'),
                 'bdd_jc': new Info(vmip: '192.168.0.20'),
                'CLUSTER_POSTGRES': new Info(vmip: '192.168.0.25'),
                'ALL_SATIRICAL': new Info(vmip: '192.168.0.25')
]


def getIP(vm_name) {
    def VM_ET = INFO_BY_APP[params.VM_NAME].vmip
    VM_IP = VM_ET
    return VM_IP
}

return this