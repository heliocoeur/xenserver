
#####install & configure ansible#####
##On the host


##install the package
yum install ansible


##create a user ansible
useradd ansible -m 

##add the user to sudo or wheel (depend of the OS)
usermod -aG sudo ansible

##create a passwd for the user ansible
passwd ansible

##create a ssh key
#login with ansible
su ansible


##On the client
#create the user ansible
useradd ansible -m

##add the user to sudo or wheel (depend of the OS)
usermod -aG sudo ansible

##create a passwd for the user ansible
passwd ansible

##create a ssh key
#login with ansible
su ansible

#create a ssh key
ssh-keygen -t rsa

ansible-vault encrypt

