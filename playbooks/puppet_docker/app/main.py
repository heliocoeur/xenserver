from flask import Flask,render_template, abort, send_file
import os
app = Flask(__name__)

@app.route('/', defaults={'req_path': '/tmp'})
@app.route('/<path:req_path>')

def dir_listing(req_path):
    BASE_DIR = '/'
    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if abs_path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if abs_path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # List and show abs_path content
    files = os.listdir(abs_path)
    return render_template('files.html', files=files)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
